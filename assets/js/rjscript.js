// const - nav
const navAbout = document.querySelector('#navAbout');
const navWork = document.querySelector('#navWork');
const navEducation = document.querySelector('#navEducation');

// const - sections
const sectAbout = document.querySelector('#about');
const sectWork = document.querySelector('#work');
const sectEducation = document.querySelector('#education');



// intro button
function introBtn() {
    // remove hash
    setTimeout(function(){
        history.replaceState("", document.title, window.location.pathname);
    }, 1);

    // nav select
    var element = document.getElementById("navAbout");
    element.classList.add("is-selected");
    navWork.classList.remove('is-selected');
    navEducation.classList.remove('is-selected');
}



// remove hash in navigation
function navRemoveHash() {
  // remove hash
  setTimeout(function(){
    history.replaceState("", document.title, window.location.pathname);
  }, 1);
}



// detection of section in viewport
document.addEventListener('scroll', function () {
    const clientHeight = document.documentElement.clientHeight;
    const aboutY = about.getBoundingClientRect().y;

    if (clientHeight >= aboutY) {

      // nav switch
      navAbout.classList.add('is-selected');
      navWork.classList.remove('is-selected');
      navEducation.classList.remove('is-selected');

      // section is in viewport
      sectAbout.classList.add('is-visible');
    }

    const workY = work.getBoundingClientRect().y;
    if (clientHeight - .5*workY >= workY) {

      // nav switch
      navAbout.classList.remove('is-selected');
      navWork.classList.add('is-selected');
      navEducation.classList.remove('is-selected');

      // section is in viewport
      sectWork.classList.add('is-visible');
    }

    const educationY = education.getBoundingClientRect().y;
    if (clientHeight - .5*educationY >= educationY) {

      // nav switch
      navAbout.classList.remove('is-selected');
      navWork.classList.remove('is-selected');
      navEducation.classList.add('is-selected');

      // section is in viewport
      sectEducation.classList.add('is-visible');
    }
});
